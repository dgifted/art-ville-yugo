import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// bootrap-vue plugin
import { BootstrapVue } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// main css file
import "./assets/css/sass/main.scss";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
